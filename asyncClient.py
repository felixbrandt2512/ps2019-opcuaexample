import logging
import asyncio
import urllib.request
from math import sin
import time

from asyncua import ua, Server
from asyncua.common.methods import uamethod


logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')


# has to be generated if http protocol
@uamethod
def getVoltage(parent):
    # REST METHODS COULD BE CALLED
    #url = "http://localhost:5000/getVoltage"
    # docker-compose 
    url = "http://rest:5000/getVoltage"
    f = urllib.request.urlopen(url)
    voltage = f.read().decode('utf-8')
    return voltage


async def main():
    ###############################
    # Setup server and namespace
    ###############################
    server = Server()
    await server.init()
    server.set_endpoint('opc.tcp://0.0.0.0:4840/freeopcua/server/')
    uri = 'http://examples.freeopcua.github.io'
    idx = await server.register_namespace(uri)
    
    ###############################
    # Import XML Model and add methods to Energiebox (Asset)
    ###############################
    await server.import_xml("./fos2019.xml")
    node = server.get_node("ns=3;i=5010")
    await node.add_method(idx,'getVoltage',getVoltage,[],[ua.VariantType.Float])
    

    ###############################
    # Start OPCUA server routine
    ###############################
    temperature = server.get_node("ns=3;i=6013")

    _logger.info('Starting server!')
    async with server:
        count = 0
        while True:
            await asyncio.sleep(0.2)
            server.set_attribute_value(temperature.nodeid, ua.DataValue(sin(time.time())))
            


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()