# ps2019-opcuaexample

In diesem Repo befinden sich:
 - eine **REST API**, die über den Endpunkt /getVoltage einen Dummywert zurück gibt.
 - ein **OPCUA Server**, der ein einfaches Modell von einem Ofen + Energiebox beschreibt.

Interessante Endpunkte sind dabei:
   - temperature //befindet sich unter Ofen->endpoints
   - getVoltage  //befindet sich unter Ofen->Energiebox->endpoints

## Installation ohne Docker
Vorrausetzung: **Python > 3**

Repo ziehen
```
git pull https://gitlab.com/felixbrandt2512/ps2019-opcuaexample.git
```

OPCUA Server starten darauf achten dass als URL in der UAmethode localhost steht
```
python asyncClient.py
```

Energiebox starten
```
cd Energiebox
python main.py
```

## Installation mit Docker
Vorraussetzung: **Docker** > 1.13 , Docker Compose

```
docker-compose build
docker-compose up
```




