from flask import (
    Flask
)

# Create the application instance
app = Flask(__name__)

# Create a URL route in our application for "/"
@app.route('/getVoltage')
def home():
    """
    Just a dummy returns string
    """
    return "230"

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0')